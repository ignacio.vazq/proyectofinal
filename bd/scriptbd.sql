CREATE DATABASE proyecto CHARACTER SET utf8 COLLATE utf8_spanish_ci;

CREATE TABLE usuario(
  id_usuario int primary key not null,
  nombre varchar(20),
  apellido1 varchar(30),
  apellido2 varchar(30),
  contraseña char(32), /* Longitud de 32 para hash MD5, se puede usar tambien binary(16) */
  crea_cuenta boolean not null default 0
)CHARACTER SET utf8 COLLATE utf8_spanish_ci;

CREATE TABLE objeto(
  id_objeto INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(30),
  tipo_objeto BOOLEAN,
  ruta longBlob,
  id_usuario INT,
  id_objeto_padre INT,
  FOREIGN KEY (id_objeto_padre) REFERENCES objeto(id_objeto), /* Referencia dentro de la misma tabla*/
  FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
)CHARACTER SET utf8 COLLATE utf8_spanish_ci;

CREATE TABLE permiso_objeto(
  id_permiso int PRIMARY KEY not null AUTO_INCREMENT,
  id_usuario int,
  id_objeto int,
  FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
  FOREIGN KEY (id_objeto) REFERENCES objeto(id_objeto)
)CHARACTER SET utf8 COLLATE utf8_spanish_ci;
